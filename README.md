SampleNodeJsClient.js
----------------------
NTMCalc authentication and REST service call demo. Shows how to authenticate at the NTMCalc Authentication Server in order to call the NTMCalc Web Service.

Implemented in NodeJS/Javascript

The demo implements the following flow:
1. The client does an initial HTTP Post request to the token endpoint at the authorization server to exchange user credentials for an access token. A new session is initated at the authorization server.

2. The authorization server responds with a JSON document containing tokens and expiration info. The client parses the JSON document to extract the tokens and the info.

3. The client calls the resource server (NTMCalc Web Service), and passes the access token as a bearer token in the HTTP authorization header. This is repeated every 30 seconds until the access token expires.

4. When the access token has expired after 5 minutes, the refresh token is used to renew the access token at the token endpoint.

5. After the final call, the client calls the logout endpoint at the authorization server to end the session.

To run the demo:

1. Install node.js and npm

2. Install the sync-request module (npm install sync-request)

3. Replace the fake credentials, "myUsername", "myPassword","myClientId" and "myClientSecret", with the credentials that you 
received in connection with the registration of your client

2016-01-07, Göran Löfgren