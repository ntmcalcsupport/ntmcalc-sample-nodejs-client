/*==================================================================================================
* SampleNodeJsClient.js
* NTMCalc authentication and REST service call demo. Implement in NodeJS/Javascript
*
* The demo implements the following flow:
* 1. The client does an initial HTTP Post request to the token endpoint at the authorization server to exchange user credentials
*    for an access token. A new session is initated at the authorization server.
* 2. The authorization server responds with a JSON document containing tokens and expiration info. The client parses the JSON documnet
*    to extract the tokens and the info.
* 3. The client calls the resource server (NTMCalc Web Service), and passes the access token as a bearer token in the HTTP authorization header.
*    This is repeated every 30 seconds until the access token expires.
* 4. When the access token has expired after 5 minutes, the refresh token is used to renew the access token at the token endpoint.
* 5. After the final call, the client calls the logout endpoint at the authorization server to end the session.
*
* To run the demo:
*   1. Install node.js and npm
*   2. Install the sync-request module (npm install sync-request)
*   3. Replace the fake credentials, "myUsername", "myPassword","myClientId" and "myClientSecret", with the credentials that you 
*      received in connection with the registration of your client
*
* 2016-01-07, Göran Löfgren
===================================================================================================*/
// Load the sync-request module to make synchronous web requests
var request = require('sync-request');  
// Init auth object to manage interactions with the authorisation server
var auth = new Auth({  
  userName: 'myUsername', // user name - replace with your own
  passWord: 'myPassword', // user password - replace with your own
  clientId: 'myClientId', // client id - replace with your own
  clientSecret: 'myClientSecret', // client secret - replace with your own
  authServer: 'test-auth.transportmeasures.org', // authorization server host
  tokenEndPointPath: '/auth/realms/ntm/protocol/openid-connect/token', // path to the token end point 
  logoutEndPointPath:'/auth/realms/ntm/protocol/openid-connect/logout' // path to the logout end point 
});
// Do a number of calls to the NTMCalc REST service
for (var i = 1; i < 15; i++) {  
  console.log('**** Call number ' + i + ' ****');
  try {
    // Get the access token needed for authentication and authorization in the web service
    var accessToken = auth.getAccessToken(); 
    // Call to NTMCalc REST service
    var res = request('POST', 'https://test-api.transportmeasures.org/v1/transportactivities',{ 
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + accessToken
      },
      json: {
        calculationObject: {  // the type of vehicle
          id: 'rigid_truck_7_5_t',
          version: '1'
        },  
        parameters: [  // parameter settings. Only a few are being used, the remaining ones will be assigned default values by the web service
          {
            id: 'distance',
            value: i,
            unit: 'km'
          },
          {
            id: 'shipment_weight',
            value: i/2,
            unit: 'tonne'
          }
        ]
      }
    });
    var response =JSON.parse(res.getBody());
    console.log('CO2 total = ' + response.resultTable.totals[response.resultTable.index.co2_total].value);  // print out some results
  } catch(err) {
    console.log(err);
    break;
  }
  var start = new Date().getTime();
  while (new Date().getTime() < start + (30 * 1000));  // delay for 30 seconds
};

try{
  var statCode = auth.endSession();
  console.log('The session was succesfully ended,  HTPP status Code ' + statCode) ;
} catch(err) {
  console.log(err);
}

/* ==================================================================================================
* Auth - manages the interaction with the authorization server
*/
function Auth (settings) {
  this.settings = settings;
  this.authResponse = undefined;
  this.accessTokenExpiration = new Expiration(); // sets initial expiration time to 0
  this.refreshTokenExpiration = new Expiration(); //sets initial expiration time to 0
  this.basicAuthorization = new Buffer(this.settings.clientId + ":" + this.settings.clientSecret).toString('base64'); // encode string for basic authorization with client id and client secret

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  * Auth.getAccessToken() - retrives an access token from the token end point (the endpoint on the authorization 
  * server where the client application exchanges user credentials (or a refresh token) for an access token).
  */
  this.getAccessToken = function() {
    var parameters;
    var authorization;
    var res;
    // At the initial acquire of an access token or when the refresh token has expired: - prepare parameters to acquire an 
    // access token with user credentials (grant_type=password). Basic authorization (using Authorization header) with client id and client secret, is done.
    if(this.refreshTokenExpiration.hasExpired()) { // token expiration time is initialized to 0 so this works also for the inital acquire
      parameters = 'grant_type=password&username=' + this.settings.userName + '&password=' + this.settings.passWord 
      console.log('Using credentials to get new access token.');
    // When access token has expired: - prepare parameters to acquire an access token with a refresh token (grant_type=refresh_token).
    } else if (this.accessTokenExpiration.hasExpired()) { 
      parameters = 'grant_type=refresh_token&refresh_token=' + this.authResponse.refresh_token;
      console.log('Using refresh token to renew expired access token.');
    // When the existing acces token is still valid: - just return it.
    } else { 
      console.log('Using the existing access token.');
      console.log("Access token expires in: " + this.accessTokenExpiration.timeToExpiration());
      console.log("Refresh token expires in: " + this.refreshTokenExpiration.timeToExpiration());
      return this.authResponse.access_token;  
    }
     // make the HTTP request to the authorization server
    var res = request('POST', 'https://' + this.settings.authServer + this.settings.tokenEndPointPath,{
        headers: {
          'Authorization': 'Basic ' + this.basicAuthorization,
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: parameters,
        timeout: 10000
      });
    // Response from authorization server
    this.authResponse = JSON.parse(res.getBody()); // Parse response body into a javascript object
    this.accessTokenExpiration.setExpirationTime(this.authResponse.expires_in); // reset expiration time for 
    this.refreshTokenExpiration.setExpirationTime(this.authResponse.refresh_expires_in);
    return this.authResponse.access_token;
  };

   /*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  * Auth.endSession() - revokes any valid refresh tokens and ends the session on the authorization server. It returns HTTP status code 204 if ok.
  */
  this.endSession = function() {
    if(!this.authResponse) {return "Never logged in"};
    var res = request('POST', 'https://' + this.settings.authServer + this.settings.logoutEndPointPath,{
      headers: {
        'Authorization': 'Basic ' + this.basicAuthorization,
       'Content-Type': 'application/x-www-form-urlencoded'
     },
      body:'refresh_token=' + this.authResponse.refresh_token 
    });
    return res.statusCode;
  };
};

/* ==================================================================================================
* Expiration - manages expiration time
*/
function Expiration() {
  this.expirationTime = 0;
  this.networkLatency = 5000; // to allow for som network latency between calls to the authorization server and the NTMCalc web service
  this.setExpirationTime = function(expiresInSeconds) {
    this.expirationTime = new Date().getTime() + (expiresInSeconds * 1000) - this.networkLatency;
  }
  this.hasExpired = function() {
    if(new Date().getTime() >= this.expirationTime) {
      return true;
    } else {
      return false;
    }
  }
   this.timeToExpiration = function() {
      return (this.expirationTime - new Date().getTime())/1000;
  }
}


